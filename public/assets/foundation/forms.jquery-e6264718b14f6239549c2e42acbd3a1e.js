/**
 * jQuery.placeholder - Placeholder plugin for input fields
 * Written by Blair Mitchelmore (blair DOT mitchelmore AT gmail DOT com)
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/).
 * Date: 2008/10/14
 *
 * @author Blair Mitchelmore
 * @version 1.0.1
 *
 **/
new function(a){a.fn.placeholder=function(b){b=b||{};var c=b.dataKey||"placeholderValue",d=b.attr||"placeholder",e=b.className||"placeholder",f=b.values||[],g=b.blockSubmit||!1,h=b.blankSubmit||!1,i=b.onSubmit||!1,j=b.value||"",k=b.cursor_position||0;return this.filter(":input").each(function(b){a.data(this,c,f[b]||a(this).attr(d))}).each(function(){a.trim(a(this).val())===""&&a(this).addClass(e).val(a.data(this,c))}).focus(function(){a.trim(a(this).val())===a.data(this,c)&&a(this).removeClass(e).val(j),a.fn.setCursorPosition&&a(this).setCursorPosition(k)}).blur(function(){a.trim(a(this).val())===j&&a(this).addClass(e).val(a.data(this,c))}).each(function(b,d){g?new function(b){a(b.form).submit(function(){return a.trim(a(b).val())!=a.data(b,c)})}(d):h?new function(b){a(b.form).submit(function(){return a.trim(a(b).val())==a.data(b,c)&&a(b).removeClass(e).val(""),!0})}(d):i&&new function(b){a(b.form).submit(i)}(d)})}}(jQuery)