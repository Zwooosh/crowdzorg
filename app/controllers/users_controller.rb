class UsersController < ApplicationController
  before_filter :authenticate_user!, :only => [:show]

  def index
    @users = User.where(:profession => [1,2])
  end

  def show
    @user = User.find(params[:id])
  end

end
