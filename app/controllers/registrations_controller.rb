class RegistrationsController < Devise::RegistrationsController
  before_filter :authenticate_user!
  protected

  def after_sign_up_path_for(resource)
    '/users/show'
  end

end