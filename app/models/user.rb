class User < ActiveRecord::Base
	has_many :tasks
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :profession, :name, :surname, :sex, :birth_date, :street, :street_number, :city, :postalcode, :email, :password, :password_confirmation, :remember_me, :about

  validates :street_number, :numericality => { :only_integer => true }
  validates :postalcode, :length => { :is => 6 }
  validates :name, :surname, :sex, :birth_date, :street, :street_number, :city, :postalcode, :presence => true
end