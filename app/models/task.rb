class Task < ActiveRecord::Base
	belongs_to :user
	belongs_to :categorie

	attr_accessible :title, :description, :time, :city, :profession, :categorie_id
end
