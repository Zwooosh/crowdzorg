class AddFieldsUsers < ActiveRecord::Migration
  def change
    add_column :users, :birth_date, :date
    add_column :users, :street, :string
    add_column :users, :street_number, :integer
    add_column :users, :city, :string
    add_column :users, :postalcode, :string
    add_column :users, :province, :string
    add_column :users, :about, :text
  end
end
