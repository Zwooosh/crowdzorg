class TypeToProfession < ActiveRecord::Migration
  def up
  	rename_column :users, :type, :profession
  end

  def down
  end
end
