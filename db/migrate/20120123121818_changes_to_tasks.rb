class ChangesToTasks < ActiveRecord::Migration
  def change
  	remove_column :tasks, :when
  	remove_column :tasks, :price
  	remove_column :tasks, :type

  	add_column :tasks, :type, :integer
  	add_column :tasks, :time, :integer

  	add_column :users, :surname, :string
  	add_column :users, :sex, :string
  	add_column :users, :type, :integer
  end
end
