class ChangingTypeToProfessionInTasks < ActiveRecord::Migration
  def up
  		rename_column :tasks, :type, :profession
  end

  def down
  end
end
