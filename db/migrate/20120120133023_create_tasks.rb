class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.date :when
      t.string :city
      t.decimal :price
      t.boolean :status
      t.boolean :type
      t.integer :user_id
      t.integer :categorie_id

      t.timestamps
    end
  end
end
