class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :helpfull
      t.integer :social
      t.integer :dedication
      t.integer :accessibility
      t.text :content
      t.integer :taks_id
      t.integer :user_id

      t.timestamps
    end
  end
end
